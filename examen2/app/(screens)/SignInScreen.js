import { router } from "expo-router";
import { Link } from "expo-router";
import { signIn } from "../index";
import { useState } from "react";
import { StyleSheet, TextInput, Button, View, Text } from "react-native";
export default function signInScreen(){
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Sign In</Text>

      <View style={styles.inputContainer}>
        <TextInput
          placeholder="Email"
          style={styles.input}
          textContentType='emailAddress'
          value={email}
          onChangeText={e => setEmail(e)}
        />

        <TextInput
          placeholder="Password"
          style={styles.input}
          type="password"
          textContentType='password'
          value={password}
          onChangeText={e => setPassword(e)}
        />

        <Button
          title='Sign In'
          onPress={() => signIn(email, password)}
        />

        <Link href="/">New here? Sign Up</Link>
      </View>

    </View>

  );


}



const styles = StyleSheet.create({

  input: {
    borderColor: "black",
    borderStyle: "solid",
    borderWidth: "1px",
  },


  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  inputContainer: {
    display: "flex",
    gap: "1rem",

  },


  title: {
      fontSize: "1.5rem",
      paddingBottom: "10px"
  },




});
