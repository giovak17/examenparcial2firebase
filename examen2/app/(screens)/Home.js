import {StyleSheet, View, Text } from "react-native";
import { Link } from "expo-router";

export default function Home(){
  return (
    <View style={styles.container}>
      <Text style={styles.welcome}>
        Welcome, you have signed in/up correctly!
      </Text>
      <Link href="/">
        Go to register form
      </Link>
    </View>
  )
}

const styles = StyleSheet.create({


  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  welcome: {
    fontSize: "3rem",
  }

});
