  // "main": "expo/AppEntry.js",
import { StyleSheet, Text, TextInput, View, Button, Input } from 'react-native';
import { useState } from 'react';
import { Link } from "expo-router";
import { router } from 'expo-router';

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "firebase/auth";
import { onAuthStateChanged } from 'firebase/auth';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAIS1AOuMfij8XgKHNq7iiBjEkb8o9ok84",
  authDomain: "examenp2-2412b.firebaseapp.com",
  projectId: "examenp2-2412b",
  storageBucket: "examenp2-2412b.appspot.com",
  messagingSenderId: "335024664518",
  appId: "1:335024664518:web:ba232fe09efba6002a0cc7",
  measurementId: "G-B7F7Q8DWKR"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth();

// createUserWithEmailAndPassword(auth, email, password)
//   .then((userCredential) => {
//     // Signed up 
//     const user = userCredential.user;
//     // ...
//   })
//   .catch((error) => {
//     const errorCode = error.code;
//     const errorMessage = error.message;
//     // ..
//   });


export function signUp(email, password){

  createUserWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Signed up 
      const user = userCredential.user;
      console.log(user);
      router.navigate("/Home");

      // ...
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      // ..
    });


}

export function signIn(email, password){

  signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Signed in 
      const user = userCredential.user;
      console.log(user);
      router.navigate("/Home");
      // ...
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
    });

}

export default function App() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  //optional might delete
  onAuthStateChanged(auth, (user) => {
  if (user) {
    const uid = user.uid;
    // ...
  } else {
    // User is signed out
    // ...
  }
});


  return (
    <View style={styles.container}>
      <Text style={styles.firebase}>Firebase</Text>
      <Text style={styles.title}>Sign Up</Text>
        

      <View style={styles.inputContainer}>
        <TextInput
          placeholder="Email"
          style={styles.input}
          textContentType='emailAddress'
          value={email}
          onChangeText={e => setEmail(e)}
        />

        <TextInput
          placeholder="Password"
          style={styles.input}
          type="password"
          textContentType='password'
          value={password}
          onChangeText={e => setPassword(e)}
        />

        <Button
          title='SignUp'
          onPress={() => signUp(email, password)}
        />

        <Link href="/SignInScreen">Already have an account? Sign In</Link>
      </View>

    </View>
  );
}


const styles = StyleSheet.create({

  input: {
    borderColor: "black",
    borderStyle: "solid",
    borderWidth: "1px",
  },


  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  inputContainer: {
    display: "flex",
    gap: "1rem",

  },

  title: {
      fontSize: "1.5rem",
      paddingBottom: "10px"
  },


  firebase: {
      fontSize: "2.5rem",
      paddingBottom: "10px"
  },


});
